package com.baopham.websocket_server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.baopham.websocket_server.entity.RoomEntity;


@Repository
public interface RoomRepository extends JpaRepository<RoomEntity, Long> {
  
  
}
