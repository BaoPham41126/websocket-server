package com.baopham.websocket_server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.baopham.websocket_server.entity.SlideEntity;

@Repository
public interface SlideRepository extends JpaRepository<SlideEntity, Long> {
  void deleteByRoomId (long roomId);
  void findByRoomId(long roomId);
  
}