package com.baopham.websocket_server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "room_entity")
public class RoomEntity {
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
    	@Id
	Long id;
	String name;
	@Column(name = "youtube_url")
	String youtubeURL;
	
	@Column(name = "youtube_url_2")
	String youtubeURL2;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getYoutubeURL() {
		return youtubeURL;
	}
	public void setYoutubeURL(String youtubeURL) {
		this.youtubeURL = youtubeURL;
	}
	public String getYoutubeURL2() {
		return youtubeURL2;
	}
	public void setYoutubeURL2(String youtubeURL2) {
		this.youtubeURL2 = youtubeURL2;
	}
}
