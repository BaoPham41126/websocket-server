package com.baopham.websocket_server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "slide_entity")
public class SlideEntity {
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
    	@Id
	Long id;
    	@Column(name = "room_id")
	Long roomId;
    	@Column(name = "slide_index")
	Long slideIndex;
	String url;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getRoomId() {
		return roomId;
	}
	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}
	public Long getSlideIndex() {
		return slideIndex;
	}
	public void setSlideIndex(Long slideIndex) {
		this.slideIndex = slideIndex;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
