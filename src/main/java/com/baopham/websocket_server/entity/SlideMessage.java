package com.baopham.websocket_server.entity;

public class SlideMessage {
	Long roomId;
	Integer slideIndex;
	String time;
	public Integer getSlideIndex() {
		return slideIndex;
	}
	public void setSlideIndex(Integer slideIndex) {
		this.slideIndex = slideIndex;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Long getRoomId() {
		return roomId;
	}
	public void setRoomId(Long roomId) {
		this.roomId = roomId;
	}
	
}
