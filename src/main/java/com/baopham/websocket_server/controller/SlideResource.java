package com.baopham.websocket_server.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.baopham.websocket_server.service.PdfService;

@RestController
@RequestMapping("/slide")
public class SlideResource {
    @Autowired
    PdfService pdfService;

    @RequestMapping(value = "/{roomId}/{slideIndex}", method = RequestMethod.POST)
    public ResponseEntity<?> insert(@PathVariable("roomId") Long roomId, @PathVariable("slideIndex") Long slideIndex,
	    @RequestParam("file") MultipartFile file) {
	System.out.println("Update slide of room: " + roomId + ": " + slideIndex + "->" + file.getName());
	return new ResponseEntity<String>("OK inserted", HttpStatus.OK);
    }   

    @RequestMapping(value = "/{roomId}", method = RequestMethod.POST)
    public ResponseEntity<?> updateSlide(@PathVariable("roomId") Long roomId, @RequestParam("file") MultipartFile file)
	    throws IOException {
	System.out.println("Update slide of room " + roomId + ": " + file.getOriginalFilename());
	File convFile = new File(file.getOriginalFilename());
	if (convFile.exists())
	    convFile.delete();
	convFile.createNewFile();
	FileOutputStream fos = new FileOutputStream(convFile);
	fos.write(file.getBytes());
	fos.close();
	pdfService.convertPdfToPic(convFile, roomId);

	return new ResponseEntity<String>("OK inserted", HttpStatus.OK);
    }
}
