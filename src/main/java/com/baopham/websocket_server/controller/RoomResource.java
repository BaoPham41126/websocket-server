package com.baopham.websocket_server.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baopham.websocket_server.entity.RoomEntity;
import com.baopham.websocket_server.repository.RoomRepository;

@RestController
@RequestMapping("/room")
public class RoomResource {
	@Autowired
	private RoomRepository roomRepo;
	@RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> all() {
		List<RoomEntity> result = roomRepo.findAll();
		
		return new ResponseEntity<List<RoomEntity>>(result, HttpStatus.OK);
    }
	
	@RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> findOne(@RequestParam("roomId") long roomId) {
		RoomEntity result = roomRepo.findOne(roomId);
		
		return new ResponseEntity<RoomEntity>(result, HttpStatus.OK);
    }

	@RequestMapping(value = "/{roomId}/clear", method = RequestMethod.DELETE)
    public ResponseEntity<?> clear(@PathVariable(value = "roomId", required = true) Long roomId) {
		System.out.println("Clear data of room " + roomId);
		return new ResponseEntity<String>("OK deleted", HttpStatus.OK);
    }
	
	@RequestMapping(value = "/{roomId}/youtubeUrl", method = RequestMethod.PUT)
    public ResponseEntity<?> updateYoutubeUrl(@PathVariable(value = "roomId", required = true) Long roomId, @RequestBody Map<String, String> youtubeUrls) {
		RoomEntity room = roomRepo.getOne(roomId);
		room.setYoutubeURL(youtubeUrls.get("1"));
		room.setYoutubeURL2(youtubeUrls.get("2"));
		roomRepo.save(room);
		
		return new ResponseEntity<String>("OK updated", HttpStatus.OK);
    }
	
}
