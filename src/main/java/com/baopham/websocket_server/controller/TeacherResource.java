package com.baopham.websocket_server.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baopham.websocket_server.entity.SlideMessage;

@RestController
public class TeacherResource {
	private SimpMessagingTemplate webSocket;
	private Double delay = null;

	@Autowired
	public TeacherResource(SimpMessagingTemplate webSocket) {
		this.webSocket = webSocket;
	}
	
	@RequestMapping(value = "/change_slide/{roomId}", method = RequestMethod.POST)
    public String receiveNewSlideIndex(@RequestBody SlideMessage slideInfo ,@PathVariable("roomId") Long roomId) {
		if (delay == null) {
	        return "Set delay first";
		}
		Map<String, Object> result = new HashMap<String, Object>();
		slideInfo.setTime(slideInfo.getTime().replace(",", "."));
		System.out.println(slideInfo.getSlideIndex() + ":" +slideInfo.getTime());
		result.put("slideIndex", slideInfo.getSlideIndex());
		result.put("time", Double.valueOf(slideInfo.getTime()) - delay);
		webSocket.convertAndSend("/room/"+roomId, result);
        return "OK";
    }
	
	@RequestMapping(value = "/delay", method = RequestMethod.POST)
    public String receiveNewSlideIndex(@RequestBody Map<String, String> data) {
		String delayString = data.get("delay").replace(",", ".");
		this.delay = Double.valueOf(delayString);
		System.out.println("YouTube started, delay = " + delay);
        return "OK";
    }
}
