package com.baopham.websocket_server.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baopham.websocket_server.entity.SlideEntity;
import com.baopham.websocket_server.repository.SlideRepository;
@Service
public class PdfService {

    private static String filePath = "/var/www/html/" ;
    @Autowired 
    private SlideRepository slideRepository;
    @Transactional
    public  void convertPdfToPic( File pdfFile , long roomId) throws IOException {
	slideRepository.deleteByRoomId(roomId);
	PDDocument document = PDDocument.load(pdfFile);
	PDFRenderer pdfRenderer = new PDFRenderer(document);
	for (int page = 0; page < document.getNumberOfPages(); ++page)
	{ 
	    BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);

	    // suffix in filename will be used as the file format
	    File f = new File(filePath+roomId + "-"+ page);
	    if(f.exists() && !f.isDirectory()) { 
	       f.delete();
	    }
	    ImageIO.write(bim, "jpg",new File(filePath+roomId + "-"+ page+".jpg")  );
	    SlideEntity newSlide = new SlideEntity();
	    newSlide.setRoomId(roomId);
	    newSlide.setSlideIndex(Long.parseLong(page+""));
	    newSlide.setUrl(filePath+roomId + "-"+ page);
	    slideRepository.save(newSlide);
	}
	document.close();
    }
}
