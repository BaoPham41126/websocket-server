package com.baopham.websocket_server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration extends AbstractWebSocketMessageBrokerConfigurer 
{
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
    	//config.enableStompBrokerRelay("/student_client");
        config.setApplicationDestinationPrefixes("/bk");
    }

    public void registerStompEndpoints(StompEndpointRegistry registry) {
         registry.addEndpoint("/bk_elearning_websocket").setAllowedOrigins("*").withSockJS();
    }
}
