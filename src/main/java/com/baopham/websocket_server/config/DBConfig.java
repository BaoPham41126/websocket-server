/*
package com.baopham.websocket_server.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory")
@PropertySource("classpath:application.properties")
public class DBConfig {
    @Autowired
    private Environment env;

    @Bean
    public NamedParameterJdbcTemplate namedParameterTemplate() {
	return new NamedParameterJdbcTemplate(localDataSource());
    }

    @Primary
    @Bean(name = "localDataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource localDataSource() {
	return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
	    @Qualifier("localDataSource") DataSource dataSource) {
	return builder.dataSource(dataSource).persistenceUnit("localDataSource")
		.packages("com.baopham.websocket_server").build();
    }

    @Primary
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(
	    @Qualifier("entityManagerFactory") EntityManagerFactory entityManagerFactory) {
	return new JpaTransactionManager(entityManagerFactory);
    }

    Properties hibernateProperties() {
	return new Properties() {
	    {
		// setProperty("hibernate.hbm2ddl.auto",
		// env.getProperty("hibernate.hbm2ddl.auto"));
		setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
		// setProperty("hibernate.globally_quoted_identifiers",
		// env.getProperty("hibernate.globally_quoted_identifiers"));
		setProperty("hibernate.jdbc.batch_size", env.getProperty("hibernate.jdbc.batch_size"));
		setProperty("hibernate.connection.url", env.getProperty("hibernate.connection.url"));
		setProperty("hibernate.connection.username", env.getProperty("hibernate.connection.username"));
		setProperty("hibernate.connection.password", env.getProperty("hibernate.connection.password"));
	    }
	};
    }

}
*/